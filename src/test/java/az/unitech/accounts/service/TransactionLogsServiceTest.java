package az.unitech.accounts.service;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import az.unitech.accounts.dao.entity.TransactionLogEntity;
import az.unitech.accounts.dao.repository.TransactionLogsRepo;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class TransactionLogsServiceTest {

    @InjectMocks
    private TransactionLogsService transactionLogsService;

    @Mock
    private TransactionLogsRepo transactionLogsRepo;

    @Test
    public void testSaveTransactionLogEntity() {
        // Create a sample TransactionLogEntity
        TransactionLogEntity transactionLogEntity = new TransactionLogEntity();
        transactionLogEntity.setId(1L); // Set an ID for the entity

        // Mock the behavior of transactionLogsRepo.save
        when(transactionLogsRepo.save(any(TransactionLogEntity.class))).thenReturn(transactionLogEntity);

        // Call the save method of the service
        TransactionLogEntity savedEntity = transactionLogsService.save(transactionLogEntity);

        // Verify that transactionLogsRepo.save was called with the correct argument
        verify(transactionLogsRepo, times(1)).save(transactionLogEntity);

        // Assert that the returned entity is not null and has the same ID
        assertNotNull(savedEntity);
        assertEquals(1L, savedEntity.getId());
    }
}
