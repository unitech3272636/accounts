package az.unitech.accounts.service;

import static org.junit.jupiter.api.Assertions.*;

import az.unitech.accounts.dao.entity.TransactionLogEntity;
import az.unitech.accounts.dto.common.AccountDto;
import az.unitech.accounts.dto.request.TransactionRequest;
import az.unitech.accounts.dto.response.CurrenciesResponse;
import az.unitech.accounts.dto.response.TransactionResponse;
import az.unitech.accounts.enums.AccountStatus;
import az.unitech.accounts.enums.BusinessResult;
import az.unitech.accounts.enums.Currency;
import az.unitech.accounts.exception.CustomException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransferServiceTest {

    @InjectMocks
    private TransferService transferService;

    @Mock
    private AccountService accountService;

    @Mock
    private TransactionLogsService transactionLogsService;

    @Mock
    private CurrencyService currencyService;

    @Test
    public void a2a_successful_transfer() {

        String userId = "user123";
        TransactionRequest request =
                new TransactionRequest("123456", "789012", Currency.USD, BigDecimal.TEN);
        AccountDto debitAccount = getAccount("123456", userId, BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        AccountDto creditAccount = getAccount("otherUser", userId, BigDecimal.valueOf(50), Currency.USD, AccountStatus.ACTIVE);

        when(accountService.findByAccountNo("123456")).thenReturn(Optional.of(debitAccount));
        when(accountService.findByAccountNo("789012")).thenReturn(Optional.of(creditAccount));
        when(transactionLogsService.save(any(TransactionLogEntity.class))).thenAnswer(invocation -> {
            TransactionLogEntity entity = invocation.getArgument(0);
            entity.setId(1L);
            return entity;
        });

        // Perform the transfer
        TransactionResponse response = transferService.a2a(userId, request);

        // Verify that the methods were called
        verify(accountService, times(1)).findByAccountNo("123456");
        verify(accountService, times(1)).findByAccountNo("789012");
        verify(transactionLogsService, times(2)).save(any(TransactionLogEntity.class));

        // Assert that the response is not null
        assertNotNull(response);

        // Add more assertions as needed to validate the response object
        assertNotNull(response.getChargeTransactionId());
        assertNotNull(response.getTopUpTransactionId());
        assertNotNull(response.getTransactionTime());
    }


    @Test
    public void amount_exchange_same_currency() {
        // Mock data for the test
        AccountDto accountDto =
                AccountDto.builder()
                        .accountNo("123456")
                        .userId("user123")
                        .balance(BigDecimal.ZERO)
                        .currency(Currency.USD)
                        .status(AccountStatus.ACTIVE)
                        .build();
        BigDecimal amount = BigDecimal.valueOf(100);
        Currency currency = Currency.USD;

        BigDecimal exchangedAmount = transferService.amountExchange(accountDto, amount, currency);

        assertEquals(amount, exchangedAmount);
    }

    @Test
    public void exchange_different_currency_to_AZN() {
        // Mock data for the test
        AccountDto accountDto =
                AccountDto.builder()
                        .accountNo("123456")
                        .userId("user123")
                        .balance(BigDecimal.ZERO)
                        .currency(Currency.USD)
                        .status(AccountStatus.ACTIVE)
                        .build();

        BigDecimal amount = BigDecimal.valueOf(100);
        Currency currency = Currency.AZN;

        CurrenciesResponse response = new CurrenciesResponse();
        response.setSaleRate(BigDecimal.valueOf(1.7));
        when(currencyService.getCurrency(any())).thenReturn(response);

        BigDecimal exchangedAmount = transferService.amountExchange(accountDto, amount, currency);

        verify(currencyService).getCurrency(Currency.USD);

        assertEquals(amount.divide(BigDecimal.valueOf(1.7), BigDecimal.ROUND_DOWN), exchangedAmount);
    }

    @Test
    public void exchange_different_currency_from_AZN() {
        AccountDto accountDto =
                AccountDto.builder()
                        .accountNo("123456")
                        .userId("user123")
                        .balance(BigDecimal.ZERO)
                        .currency(Currency.AZN)
                        .status(AccountStatus.ACTIVE)
                        .build();
        BigDecimal amount = BigDecimal.valueOf(100);
        Currency currency = Currency.USD;

        CurrenciesResponse response = new CurrenciesResponse();
        response.setBuyRate(BigDecimal.valueOf(1.7));
        when(currencyService.getCurrency(any())).thenReturn(response);

        BigDecimal exchangedAmount = transferService.amountExchange(accountDto, amount, currency);

        verify(currencyService).getCurrency(Currency.USD);

        assertEquals(amount.multiply(BigDecimal.valueOf(1.7)), exchangedAmount);
    }

    @Test
    public void testAmountExchange_ConvertToAZN() {
        AccountDto accountDto =
                AccountDto.builder()
                        .accountNo("123456")
                        .userId("user123")
                        .balance(BigDecimal.ZERO)
                        .currency(Currency.USD)
                        .status(AccountStatus.ACTIVE)
                        .build();
        BigDecimal amount = BigDecimal.valueOf(100);
        Currency currency = Currency.AZN;

        // Mock behavior of currencyService
        CurrenciesResponse response1 = new CurrenciesResponse();
        response1.setSaleRate(BigDecimal.valueOf(1.7)); // Mock a sale rate for USD to AZN
        when(currencyService.getCurrency(Currency.USD)).thenReturn(response1);

        // Perform the exchange
        BigDecimal exchangedAmount = transferService.amountExchange(accountDto, amount, currency);

        // Verify that currencyService.getCurrency was called with the correct argument
        verify(currencyService).getCurrency(Currency.USD);

        // The amount should be divided by the sale rate (1.7 in this case)
        assertEquals(amount.divide(BigDecimal.valueOf(1.7), BigDecimal.ROUND_DOWN), exchangedAmount);
    }

    @Test
    public void testMatchCurrency_CurrencyMatched() {

        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        Currency currency = Currency.USD;

        transferService.matchCurrency(accountDto, currency);
    }

    @Test
    public void testMatchCurrency_CurrencyNotMatched() {
        // Create a sample AccountDto and Currency with different currencies
        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        Currency currency = Currency.EUR;

        // Call the matchCurrency method and expect a CustomException
        CustomException customException = assertThrows(CustomException.class,
                () -> transferService.matchCurrency(accountDto, currency));

        // Verify that the CustomException has the expected BusinessResult
        assertEquals(BusinessResult.CURRENCY_NOT_MATCHED, customException.getBusinessResultEnum());
    }

    @Test
    public void testCheckAccountsAreEquals_AccountsNotEqual() {

        String drAccount = "account123";
        String crAccount = "otherAccount";

        transferService.checkAccountsAreEquals(drAccount, crAccount);
    }

    @Test
    public void testCheckAccountsAreEquals_AccountsEqual() {
        String drAccount = "account123";
        String crAccount = "account123";

        CustomException customException = assertThrows(CustomException.class,
                () -> transferService.checkAccountsAreEquals(drAccount, crAccount));

        assertEquals(BusinessResult.IDENTICAL_ACCOUNT, customException.getBusinessResultEnum());
    }

    @Test
    public void testCheckBlockOnAccount_AccountActive() {

        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);

        transferService.checkBlockOnAccount(accountDto);
    }

    @Test
    public void testCheckBlockOnAccount_AccountBlocked() {

        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.BLOCKED);

        CustomException customException = assertThrows(CustomException.class,
                () -> transferService.checkBlockOnAccount(accountDto));

        assertEquals(BusinessResult.ACCOUNT_BLOCKED, customException.getBusinessResultEnum());
    }

    @Test
    public void testIdentifyAccountWithUserId_AccountIdentified() {

        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        String userId = "user123";

        transferService.identifyAccountWithUserId(accountDto, userId);
    }

    @Test
    public void testIdentifyAccountWithUserId_AccountNotIdentified() {

        AccountDto accountDto = getAccount("123456", "otherUser", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        String userId = "user123";

        CustomException customException = assertThrows(CustomException.class,
                () -> transferService.identifyAccountWithUserId(accountDto, userId));

        assertEquals(BusinessResult.IDENTIFY_DEBIT_ACCOUNT, customException.getBusinessResultEnum());
    }

    @Test
    public void testCheckBalance_EnoughBalance() {

        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        BigDecimal amount = BigDecimal.valueOf(50);

        transferService.checkBalance(accountDto, amount);
    }

    @Test
    public void testCheckBalance_InsufficientBalance() {

        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        BigDecimal amount = BigDecimal.valueOf(150);

        CustomException customException = assertThrows(CustomException.class,
                () -> transferService.checkBalance(accountDto, amount));

        assertEquals(BusinessResult.INSUFFICIENT_BALANCE, customException.getBusinessResultEnum());
    }

    @Test
    public void testAmountExchange_SameCurrency() {

        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        Currency currency = Currency.USD;
        BigDecimal amount = BigDecimal.valueOf(50);

        BigDecimal exchangedAmount =
                transferService.amountExchange(accountDto, amount, currency);

        assertEquals(amount, exchangedAmount);
    }

    @Test
    public void testAmountExchange_ToAZN() {
        // Create a sample AccountDto and Currency
        AccountDto accountDto = getAccount("123456", "user123", BigDecimal.valueOf(100), Currency.USD, AccountStatus.ACTIVE);
        Currency currency = Currency.AZN;
        BigDecimal amount = BigDecimal.valueOf(50);

        CurrenciesResponse usdToAznResponse = new CurrenciesResponse();
        usdToAznResponse.setSaleRate(BigDecimal.valueOf(1.7)); // Replace with your actual conversion rate
        when(currencyService.getCurrency(Currency.USD)).thenReturn(usdToAznResponse);

        BigDecimal exchangedAmount = transferService.amountExchange(accountDto, amount, currency);

        BigDecimal expectedExchangedAmount = amount.divide(usdToAznResponse.getSaleRate(), RoundingMode.DOWN);

        assertEquals(expectedExchangedAmount, exchangedAmount);
    }

    public AccountDto getAccount(String accountNo, String userId, BigDecimal balance, Currency currency, AccountStatus accountStatus) {
        return AccountDto.builder()
                .accountNo(accountNo)
                .userId(userId)
                .balance(balance)
                .currency(currency)
                .status(accountStatus)
                .build();
    }
}
