package az.unitech.accounts.service;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import az.unitech.accounts.client.CurrencyClient;
import az.unitech.accounts.dto.response.CurrenciesResponse;
import az.unitech.accounts.enums.BusinessResult;
import az.unitech.accounts.enums.Currency;
import az.unitech.accounts.exception.CustomException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CurrencyServiceTest {

    @InjectMocks
    private CurrencyService currencyService;

    @Mock
    private CurrencyClient currencyClient;

    @Test
    public void testGetCurrency_Success() {
        Currency currencyCode = Currency.USD;

        CurrenciesResponse usdResponse = new CurrenciesResponse();
        usdResponse.setCode(Currency.USD.name());
        usdResponse.setBuyRate(BigDecimal.valueOf(1.0)); // Replace with your expected buy rate
        usdResponse.setSaleRate(BigDecimal.valueOf(1.1)); // Replace with your expected sale rate

        when(currencyClient.getCurrency(currencyCode.name())).thenReturn(Optional.of(usdResponse));

        CurrenciesResponse response = currencyService.getCurrency(currencyCode);

        assertEquals(usdResponse, response);

    }

    @Test
    public void testGetCurrency_UnknownCurrency() {
        // Define a test currency code for which the currency is unknown
        Currency currencyCode = Currency.UNKNOWN;

        // Mock the behavior of currencyClient to return an empty Optional, simulating an unknown currency
        when(currencyClient.getCurrency(currencyCode.name())).thenReturn(Optional.empty());

        // Call the getCurrency method and expect a CustomException
        assertThrows(CustomException.class, () -> currencyService.getCurrency(currencyCode));
    }


}
