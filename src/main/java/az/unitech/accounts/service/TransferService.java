package az.unitech.accounts.service;

import az.unitech.accounts.dao.entity.TransactionLogEntity;
import az.unitech.accounts.dto.common.AccountDto;
import az.unitech.accounts.dto.request.TransactionRequest;
import az.unitech.accounts.dto.response.CurrenciesResponse;
import az.unitech.accounts.dto.response.TransactionResponse;
import az.unitech.accounts.enums.*;
import az.unitech.accounts.exception.CustomException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class TransferService {

    private final AccountService accountService;
    private final TransactionLogsService transactionLogsService;
    private final CurrencyService currencyService;

    @Transactional
    public TransactionResponse a2a(String userId, TransactionRequest request) {
        checkAccountsAreEquals(request.getDrAccount(), request.getCrAccount());
        AccountDto debitAccount = accountService.findByAccountNo(request.getDrAccount())
                .orElseThrow(() -> CustomException.of(BusinessResult.ACCOUNT_NOT_FOUND));
        matchCurrency(debitAccount, request.getCurrency());
        identifyAccountWithUserId(debitAccount, userId);
        AccountDto creditAccount = accountService.findByAccountNo(request.getCrAccount())
                .orElseThrow(() -> CustomException.of(BusinessResult.ACCOUNT_NOT_FOUND));
        checkBlockOnAccount(creditAccount);
        checkBlockOnAccount(debitAccount);
        TransactionLogEntity chargeTransaction =
                chargeFromAccount(debitAccount, creditAccount.getAccountNo(), request.getAmount());
        TransactionLogEntity topUpTransaction =
                topUpToAccount(creditAccount, debitAccount.getAccountNo(), request.getAmount(), request.getCurrency());
        return TransactionResponse.builder()
                .chargeTransactionId(chargeTransaction.getId())
                .topUpTransactionId(topUpTransaction.getId())
                .transactionTime(LocalDateTime.now())
                .build();
    }

    private TransactionLogEntity chargeFromAccount(AccountDto drAccount, String crAccountNo, BigDecimal amount) {
        checkBalance(drAccount, amount);
        drAccount.setBalance(drAccount.getBalance().subtract(amount));
        accountService.save(drAccount);
        return transactionLogsService.save(
                TransactionLogEntity.builder()
                        .drAccount(drAccount.getAccountNo())
                        .crAccount(crAccountNo)
                        .currency(drAccount.getCurrency())
                        .amount(amount)
                        .transferType(TransferType.CHARGE)
                        .status(TransferStatus.SUCCESS)
                        .build());
    }

    private TransactionLogEntity topUpToAccount(AccountDto crAccount, String dtAccountNo, BigDecimal amount, Currency currency) {
        BigDecimal finalAmount = amountExchange(crAccount, amount, currency);
        crAccount.setBalance(crAccount.getBalance().add(finalAmount));
        accountService.save(crAccount);
        return transactionLogsService.save(
                TransactionLogEntity.builder()
                        .drAccount(dtAccountNo)
                        .crAccount(crAccount.getAccountNo())
                        .currency(crAccount.getCurrency())
                        .amount(finalAmount)
                        .transferType(TransferType.TOP_UP)
                        .status(TransferStatus.SUCCESS)
                        .build());
    }

    void matchCurrency(AccountDto accountDto, Currency currency) {
        if (accountDto.getCurrency() != currency) {
            throw CustomException.of(BusinessResult.CURRENCY_NOT_MATCHED);
        }
    }

    void checkAccountsAreEquals(String drAccount, String crAccount) {
        if (drAccount.equalsIgnoreCase(crAccount))
            CustomException.of(BusinessResult.IDENTICAL_ACCOUNT).throwEx();
    }

    void checkBlockOnAccount(AccountDto accountDto) {
        if (!accountDto.getStatus().equals(AccountStatus.ACTIVE))
            CustomException.of(BusinessResult.ACCOUNT_BLOCKED).throwEx();
    }

    void identifyAccountWithUserId(AccountDto dto, String userId) {
        if (!dto.getUserId().equalsIgnoreCase(userId)) {
            throw CustomException.of(BusinessResult.IDENTIFY_DEBIT_ACCOUNT);
        }
    }

    void checkBalance(AccountDto dto, BigDecimal amount) {
        if (dto.getBalance().compareTo(amount) < 0) {
            throw CustomException.of(BusinessResult.INSUFFICIENT_BALANCE);
        }
    }

    BigDecimal amountExchange(AccountDto dto, BigDecimal amount, Currency currency) {
        if (currency == dto.getCurrency()) {
            return amount;
        } else {
            if(currency == Currency.AZN){
                CurrenciesResponse response1 = currencyService.getCurrency(dto.getCurrency());
                return amount.divide(response1.getSaleRate(), RoundingMode.DOWN);
            }
            CurrenciesResponse response = currencyService.getCurrency(currency);
            BigDecimal multiply = amount.multiply(response.getBuyRate());
            if (dto.getCurrency() == Currency.AZN) {
                return multiply;
            }else {
                CurrenciesResponse response2 = currencyService.getCurrency(dto.getCurrency());
                return multiply.divide(response2.getSaleRate(), RoundingMode.DOWN);
            }
        }
    }
}
