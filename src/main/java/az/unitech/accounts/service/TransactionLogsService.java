package az.unitech.accounts.service;

import az.unitech.accounts.dao.entity.TransactionLogEntity;
import az.unitech.accounts.dao.repository.TransactionLogsRepo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class TransactionLogsService {

    private final TransactionLogsRepo transactionLogsRepo;

    public TransactionLogEntity save(TransactionLogEntity entity){
        return transactionLogsRepo.save(entity);
    }
}
