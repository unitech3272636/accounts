package az.unitech.accounts.service;

import az.unitech.accounts.client.CurrencyClient;
import az.unitech.accounts.dto.response.CurrenciesResponse;
import az.unitech.accounts.enums.BusinessResult;
import az.unitech.accounts.enums.Currency;
import az.unitech.accounts.exception.CustomException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CurrencyService {
    private final CurrencyClient currencyClient;

    public CurrenciesResponse getCurrency(Currency currencyCode) {
        return currencyClient.getCurrency(currencyCode.name())
                .orElseThrow(() -> CustomException.of(BusinessResult.UNKNOWN_CURRENCY));
    }
}
