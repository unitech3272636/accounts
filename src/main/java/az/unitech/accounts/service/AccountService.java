package az.unitech.accounts.service;

import az.unitech.accounts.dao.entity.AccountEntity;
import az.unitech.accounts.dao.repository.AccountRepository;
import az.unitech.accounts.dto.common.AccountDto;
import az.unitech.accounts.enums.BusinessResult;
import az.unitech.accounts.exception.CustomException;
import az.unitech.accounts.mapping.AccountMapping;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapping accountMapping;

    public AccountDto findUserAccountsByUserPin(String userPin) {
        return accountRepository.findByUserId(userPin).map(accountMapping::toDto)
                .orElseThrow(() -> CustomException.of(BusinessResult.ACCOUNT_NOT_FOUND));
    }

    public Optional<AccountDto> findByAccountNo(String accountNo) {
        return accountRepository.findByAccountNo(accountNo)
                .map(accountMapping::toDto);
    }

    public AccountDto save(AccountDto dto) {
        AccountEntity entity = accountMapping.toEntity(dto);
        return accountMapping.toDto(accountRepository.save(entity));
    }
}
