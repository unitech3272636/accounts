package az.unitech.accounts.controller;

import az.unitech.accounts.dto.common.AccountDto;
import az.unitech.accounts.dto.request.TransactionRequest;
import az.unitech.accounts.dto.response.TransactionResponse;
import az.unitech.accounts.service.AccountService;
import az.unitech.accounts.service.TransferService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Validated
@RestController
@AllArgsConstructor
public class AccountController {

    private final AccountService accountService;
    private final TransferService transferService;

    @GetMapping("/info")
    public AccountDto accountInfo(@RequestHeader Map<String, String> headers) {
        return accountService.findUserAccountsByUserPin(headers.get("x-user-id"));
    }

    @PostMapping("account-to-account")
    public TransactionResponse accountInfo(@RequestHeader Map<String, String> headers,
                                           @RequestBody TransactionRequest request) {
        return transferService.a2a(headers.get("x-user-id"), request);
    }
}
