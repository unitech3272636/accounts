package az.unitech.accounts.mapping;

import az.unitech.accounts.dao.entity.AccountEntity;
import az.unitech.accounts.dto.common.AccountDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapping {
        AccountDto toDto(AccountEntity accountEntity);

        AccountEntity toEntity(AccountDto dto);
}
