package az.unitech.accounts.dto.request;

import az.unitech.accounts.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionRequest {
    private String drAccount;
    private String crAccount;
    private Currency currency;
    private BigDecimal amount;
}
