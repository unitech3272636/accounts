package az.unitech.accounts.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionResponse implements Serializable {
    private Long chargeTransactionId;
    private Long topUpTransactionId;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:yy")
    private LocalDateTime transactionTime;
}
