package az.unitech.accounts.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CurrenciesResponse implements Serializable {
    private String code;

    private BigDecimal buyRate;

    private BigDecimal saleRate;

    private LocalDateTime createdDate;

    private LocalDateTime updatedDate;
}