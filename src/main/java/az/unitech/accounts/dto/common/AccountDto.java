package az.unitech.accounts.dto.common;

import az.unitech.accounts.enums.AccountStatus;
import az.unitech.accounts.enums.Currency;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class AccountDto {
    private Long id;
    private String userId;
    private Currency currency;
    private String accountNo;
    private BigDecimal balance;
    private AccountStatus status;
}