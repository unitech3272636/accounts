package az.unitech.accounts.dto.common;

import az.unitech.accounts.enums.BusinessResult;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommonErrorResponse {
    private Long code;
    private String systemMessage;
    private String message;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime time;

    public static CommonErrorResponse newInstance(BusinessResult businessResult) {
        return CommonErrorResponse.builder()
                .code(Long.valueOf(businessResult.getCode()))
                .systemMessage("ERROR")
                .message(businessResult.getMessage())
                .time(LocalDateTime.now())
                .build();
    }

    public static CommonErrorResponse newInstance(String systemMessage, BusinessResult businessResult) {
        return CommonErrorResponse.builder()
                .code(Long.valueOf(businessResult.getCode()))
                .systemMessage(systemMessage)
                .message(businessResult.getMessage())
                .time(LocalDateTime.now())
                .build();
    }
}