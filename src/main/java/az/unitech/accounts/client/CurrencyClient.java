package az.unitech.accounts.client;

import az.unitech.accounts.dto.response.CurrenciesResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@FeignClient(name = "currency-v1", url = "${url.currency-service}")
public interface CurrencyClient {
    @GetMapping(value = "/currencies")
    Optional<CurrenciesResponse> getCurrency(@RequestParam String code);
}
