package az.unitech.accounts.exception;


import az.unitech.accounts.enums.BusinessResult;

import java.io.Serializable;

public class CustomException extends RuntimeException implements Serializable {

    private BusinessResult businessResult;

    public BusinessResult getBusinessResultEnum() {
        return businessResult;
    }

    public CustomException(BusinessResult businessResult) {
        this.businessResult = businessResult;
    }

    public static CustomException of(BusinessResult businessResult) {
        return new CustomException(businessResult);
    }

    public void throwEx() {
        throw this;
    }
}
