package az.unitech.accounts.exception;

import az.unitech.accounts.dto.common.CommonErrorResponse;
import az.unitech.accounts.enums.BusinessResult;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
@AllArgsConstructor
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public final CommonErrorResponse handleUnknownException(Exception ex) {
        return CommonErrorResponse.newInstance(ex.getMessage(), BusinessResult.INTERNAL_ERROR);
    }

    @ExceptionHandler(CustomException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public final CommonErrorResponse handleBusinessException(CustomException ex) {
        return CommonErrorResponse.newInstance( ex.getBusinessResultEnum());
    }
}
