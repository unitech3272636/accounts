package az.unitech.accounts.dao.entity;


import az.unitech.accounts.enums.AccountStatus;
import az.unitech.accounts.enums.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "accounts")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userId;
    @Enumerated(EnumType.STRING)
    private Currency currency;
    private String accountNo;
    private BigDecimal balance;
    @Enumerated(EnumType.STRING)
    private AccountStatus status;

}
