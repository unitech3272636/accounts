package az.unitech.accounts.dao.repository;

import az.unitech.accounts.dao.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    Optional<AccountEntity> findByUserId(String userPin);

    Optional<AccountEntity> findByAccountNo(String accountNo);
}
