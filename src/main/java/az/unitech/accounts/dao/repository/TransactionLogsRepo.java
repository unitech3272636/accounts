package az.unitech.accounts.dao.repository;

import az.unitech.accounts.dao.entity.TransactionLogEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionLogsRepo extends JpaRepository<TransactionLogEntity, Long> {
}
