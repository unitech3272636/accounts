package az.unitech.accounts.enums;

public enum TransferType {
    CHARGE, TOP_UP
}
