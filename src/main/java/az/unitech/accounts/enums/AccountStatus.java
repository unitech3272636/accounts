package az.unitech.accounts.enums;

public enum AccountStatus {
    ACTIVE, NONACTIVE, BLOCKED
}