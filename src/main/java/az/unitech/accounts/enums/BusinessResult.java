package az.unitech.accounts.enums;

import org.springframework.http.HttpStatus;

public enum BusinessResult {

    USER_BLOCKED(1001, HttpStatus.LOCKED, "User Blocked"),
    ACCOUNT_BLOCKED(1002, HttpStatus.LOCKED, "Account Blocked"),
    INSUFFICIENT_BALANCE(1003, HttpStatus.BAD_REQUEST, "Insufficient balance"),
    UNKNOWN_CURRENCY(1004, HttpStatus.BAD_REQUEST, "Unknown Currency"),
    ACCOUNT_NOT_FOUND(1005, HttpStatus.NOT_FOUND, "Account Not Found"),
    IDENTICAL_ACCOUNT(1006, HttpStatus.BAD_REQUEST, "Accounts cannot be the same"),
    CURRENCY_NOT_MATCHED(1007, HttpStatus.BAD_REQUEST, "Currency not belonged to account"),
    IDENTIFY_DEBIT_ACCOUNT(1008, HttpStatus.BAD_REQUEST, "Debit account is not belong to user id"),
    INTERNAL_ERROR(999, HttpStatus.INTERNAL_SERVER_ERROR, "General Error");

    private final Integer code;
    private final HttpStatus httpStatus;
    private final String message;

    BusinessResult(Integer code, HttpStatus httpStatus, String message) {
        this.code = code;
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }
}
