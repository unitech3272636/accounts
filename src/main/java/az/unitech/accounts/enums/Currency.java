package az.unitech.accounts.enums;

public enum Currency {
    AZN, USD, EUR, UNKNOWN
}
